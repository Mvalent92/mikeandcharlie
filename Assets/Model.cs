﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;
using System;


public class Model : TimestepModelAbstract {
    //namespace for all of the DLL imports in a C library

	[DllImport("UnityDLLExample",EntryPoint="setMass")]
	private static extern void setMass (IntPtr p, double value);

	[DllImport("UnityDLLExample",EntryPoint="setMasses")]
	private static extern void setMasses (IntPtr p, double [] value);

	[DllImport("UnityDLLExample",EntryPoint="setG")]
	private static extern void setG (IntPtr p, double value);

	[DllImport("UnityDLLExample",EntryPoint="setShield")]
	private static extern void setShield (IntPtr p, double value);

	[DllImport("UnityDLLExample",EntryPoint="setX")]
	private static extern void setX (IntPtr p, double value, int i);

	[DllImport("UnityDLLExample",EntryPoint="setXArray")]
	private static extern void setXArray (IntPtr p, double [] value);

    [DllImport("UnityDLLExample", EntryPoint = "setTStep")]
    private static extern void setTStep(IntPtr p, double dt);

    [DllImport("UnityDLLExample",EntryPoint="getX")]
	private static extern double getX (IntPtr p,int i);

	[DllImport("UnityDLLExample",EntryPoint="getXArray")]
	private static extern IntPtr getXArray (IntPtr p);

	[DllImport("UnityDLLExample",EntryPoint="allocModel")]
	private static extern IntPtr allocModel (int i);

	[DllImport("UnityDLLExample",EntryPoint="initModel")]
	private static extern void initModel (IntPtr p);

	[DllImport("UnityDLLExample",EntryPoint="stepModelRK2")]
	private static extern void stepModelRK2 (IntPtr p, double dt);

	[DllImport("UnityDLLExample",EntryPoint="stepModelRK4")]
	private static extern void stepModelRK4 (IntPtr p, double dt);

	[DllImport("UnityDLLExample",EntryPoint="stepModelEuler")]
	private static extern void stepModelEuler (IntPtr p, double dt);

    //Allows for the change of G based on scale of project
    [DllImport("UnityDLLExample", EntryPoint = "getGFromSI")]
    private static extern double getGFromSI(double mass, double length, double dt);

    //Leapfrog integration test
    [DllImport("UnityDLLExample", EntryPoint = "stepNbodyModelLeapfrog")]
    private static extern void stepNbodyModelLeapfrog(IntPtr p, double dt);

    //ABM integration test
    [DllImport("UnityDLLExample", EntryPoint = "stepNbodyModelABM")]
    private static extern void stepNbodyModelABM(IntPtr p, double dt);

    NBodyScript theModel;
    //Setting values for G scale
    public double setGMass = 5.972 * Math.Pow(10, 24);
    public double setGSize = 1.49 * Math.Pow(10, 11);
    public double setGTime = 86400;

	public int n=1000;
	GameObject [] theObjects;
	GameObject theParent;
	IntPtr pluginModel;
	double [] marshalledX;
	public bool useDLL=true;

	public bool drawPixels = true;
	public bool drawSpheres = true;
	public float sphereSize = 0.1f;

	public Material PCMat;
	public Material sphereMat;

    //add softening potentials here 
	public float shieldRadius = 1.0e-3f;
	public float totalMass = 1.0f;
	public float NewtonsG = 1.0f;

	void Start() { 
		theModel = new NBodyScript ();
		theModel.AllocNBS (n);
		theModel.m = totalMass;
		theModel.G = NewtonsG;
		theModel.shield = shieldRadius;
		theModel.InitNBS2 (); //initial creation of positions and velocities of system
		if (useDLL) { // goes to C code 
			pluginModel = allocModel (n);  //create model and allocate memory in C
			marshalledX = new double[6 * n];
            setShield (pluginModel, shieldRadius);
            setG(pluginModel, theModel.G);
			//setG (pluginModel, getGFromSI(setGMass,setGSize,setGTime)); //Sets G based on scale
			setMasses (pluginModel, theModel.mass);
			setXArray (pluginModel, theModel.x);
			//initModel (pluginModel);
		} 

        //in world of data io extras 

		theObjects = new GameObject[n];
		theParent = new GameObject();
		for (int i = 0; i < n; i++) {
			theObjects [i] = GameObject.CreatePrimitive (PrimitiveType.Sphere);
			theObjects [i].GetComponent<Collider> ().enabled = false;
			theObjects [i].GetComponent<Renderer> ().material = sphereMat;
			theObjects[i].transform.parent = theParent.transform;
		}
		/*
		if (useDLL) {
			for (int i = 0; i < n; i++) {
				bool inside = false;
				while (!inside) {
					float R = 2.0f;
					marshalledX [i * 6 + 0] = (double)UnityEngine.Random.Range (-R, R);
					marshalledX [i * 6 + 1] = (double)UnityEngine.Random.Range (-R, R);
					marshalledX [i * 6 + 2] = (double)UnityEngine.Random.Range (-R, R);
					double x = marshalledX [i * 6 + 0];
					double y = marshalledX [i * 6 + 1];
					double z = marshalledX [i * 6 + 2];
					double r = Mathd.Sqrt (x * x + y * y + z * z);
					if (r <= R)
						inside = true;
					double v = 1.0;
					marshalledX [i * 6 + 3] = -v * z / r;
					marshalledX [i * 6 + 4] = 0.0f;
					marshalledX [i * 6 + 5] = v * x / r;
				}

			}
		

			setXArray (pluginModel, marshalledX, 6 * n);
		}
	*/
		ModelStart ();

	}

	// Update is called once per frame
	void Update () {
		
		double[] x = new double[6*n];
		if (useDLL) {
			for (int i = 0; i < 6*n; i++) {
				x[i]= getX(pluginModel,i);
			}
			//IntPtr foou = getXArray(pluginModel);
			//Marshal.Copy (foou, marshalledX, 0, 6*n);
			//x = marshalledX;
		} else {
			x = theModel.x;
		}
		Vector3[] test = new Vector3[n];
		Color[] cols = new Color[n];
		for (int i = 0; i < n; i++) { //changes the game objects position
			theObjects [i].transform.position = new Vector3 (
				(float)x [i * 6 + 0], 
				(float)x [i * 6 + 1], 
				(float)x [i * 6 + 2]);
			test [i] = theObjects [i].transform.position;
			Vector3 v = new Vector3 ((float)x [i * 6 + 3], (float)x [i * 6 + 4], (float)x [i * 6 + 5]);
			float cscale = v.magnitude;
			cols[i] = new Color (cscale, 1.0f, 1.0f - cscale);
			theObjects [i].GetComponent<Renderer> ().material.color = cols [i];
			theObjects [i].transform.localScale = sphereSize * Vector3.one;

			if(!drawSpheres) theObjects [i].SetActive (false);
			else theObjects [i].SetActive (true);
		}
		if (drawPixels) {
			Mesh mesh = CreatePointMesh (test);
			mesh.colors = cols;
			Graphics.DrawMesh (mesh, Vector3.zero,
				Quaternion.identity, PCMat, 0);
		}
	}

	Mesh CreatePointMesh(Vector3[] points)
	{
		Mesh mesh = new Mesh();
		mesh.vertices = points;
		// You can also apply UVs or vertex colours here.

		int[] indices = new int[points.Length];
		for(int i = 0; i < points.Length; i++)
			indices[i] = i;

		mesh.SetIndices(indices, MeshTopology.Points, 0);

		return mesh;
	}

	override public void takeStep (float dt)
	{
		if (useDLL) {
            stepModelRK4 (pluginModel,(double)dt);
		} else {
			theModel.RK4Step (theModel.x, dt);

		}

		modelT += dt;
	}
}
//Seeding random number generator to 0 and look at differences in ABM and RK4
